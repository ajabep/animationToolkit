QUnit.test("Test parameters", function( assert ) {
	var i = -1, 
	    duration = 1000,
	    ease = function(percent) {
		return percent;
	    },
	    lastTimeCpt = 0,
	    done = assert.async();
	    doneFnc = function () {
		assert.ok(lastTimeCpt == 1, "The animation must stop after the duration. It can be launched, at most 1 times after the duration, to launch the 100% state");
		assert.ok(anim.isStoped(), "isStoped function must say if the animation is stoped");
		done();
	    },
	    start = new Date();

	anim = animationToolkit(function(opt) {
		assert.ok(opt.percent <= 1, "The percentage must be <= 1");
		assert.ok(opt.percent >= 0, "The percentage must be >= 0");
		assert.ok(opt.percent > i, "The current percentage must be > than the previous.");
		i = opt.percent;
		assert.ok(opt.start - start < 10, "The start value must be a Date object representing the start time.");
		assert.equal(opt.duration, duration, "The duration value must be the expected duration.");
		assert.equal(opt.ease, ease, "The ease value must be the expected ease function.");
		assert.equal(opt.done, doneFnc, "The done value must be the expected done function.");
		assert.notOk(anim.isStoped(), "isStoped function must say if the animation is stoped");

		if ((new Date()- opt.start) >= opt.duration) {
			++lastTimeCpt;
		}

		assert.ok(lastTimeCpt <= 1, "The animation must stop after the duration. It can be launched, at most 1 times after the duration, to launch the 100% state");
	}, duration, ease, doneFnc);

	assert.ok(typeof anim == "object", "The return value must be an object");
	assert.ok(typeof anim.stop == "function", "The return value must have a 'stop' key with a function to stop the animation");
});


QUnit.module("Test running", function() {
	QUnit.test("Can be stoped", function( assert ) {
		var i = 0;
		anim = animationToolkit(function(opt) {
			i = opt.percent;
		}, 1000);

		assert.notOk(anim.isStoped(), "isStoped function must say if the animation is stoped");
		anim.stop()
		assert.ok(anim.isStoped(), "isStoped function must say if the animation is stoped");

		assert.ok(i < 200, "The animation can be stoped manualy");
	});
	QUnit.test("The default ease function", function( assert ) {
		var done = assert.async();
		var anim = animationToolkit(function(opt) {
			assert.equal(opt.ease(0), 0, "The start state of the default ease must be 0");
			assert.equal(opt.ease(1), 1, "The end state of the default ease must be 1");
			assert.equal(opt.ease(.5), .5, "The middle state of the default ease must be .5");
			assert.equal(opt.ease(.25), .25, "The default ease must be a linear function");
			assert.equal(opt.ease(.75), .75, "It must be  ease must be a linear function");
			assert.notOk(anim.isStoped(), "isStoped function must say if the animation is stoped");
			anim.stop();
			assert.ok(anim.isStoped(), "isStoped function must say if the animation is stoped");

			done();
		}, 1000);
	});
});





