

'use strict';


/**
 * Make an animation, and is optimised to be light
 * 
 * @param stepCallback	Function	Called for each step.
 * 				This : null
 * 				Arguments: options	an array with, as key:
 * 					* start: a Date object which represent the time when the animation is launched.
 * 					* duration: same as the parameter duration
 * 					* ease: same as the parameter ease
 * 					* percent: the percentage where we are. eg.: .5 means that we are at the middle of the duration.
 * @param duration	Integer		Represent the duration of the animation in miliseconds.
 * @param ease		Function	Default : a linear ease
 * 				Arguments: percent: the percentage where we are. eg.: .5 means that we are at the middle of the duration.
 * @param done		Function	The ending callback ; Default : function(){}
 * 				Arguments: none
 * 
 * @return Object
 * 	Keys:
 * 		* stop: a function to stop the animation
 * 		* isStoped: a function to know if the animation is stoped
 */
var animationToolkit = function (stepCallback, duration, ease, done) {
	'use strict';

	if ((stepCallback | null) == null) {
		console.error('The callback to call at each step can\'t be null !');
		return;
	}

	if (!stepCallback instanceof Function) {
		console.error('The callback must be a function !');
		return;
	}

	if ((duration | null) == null) {
		console.error('The duration can\'t be null !');
		return;
	}

	if (!duration instanceof Number) {
		console.error('The duration must be an int !');
		return;
	}

	if (duration <= 0) {
		console.error('The duration must be positive and more than 0 !');
		return;
	}

	ease = ease || function (percent) {
		return percent;
	};

	if (!ease instanceof Function) {
		console.error('The ease must be a function !');
		return;
	}

	done = done || function () {};

	if (!done instanceof Function) {
		console.error('The done must be a function !');
		return;
	}

	var options = {
		start: new Date(),
		duration: duration,
		ease: ease,
		done: done
	};

	var requestID,
	    stop = false,
	    launchEnding = false;

	var frame = function() {
		var deltaTime = new Date() - options.start; // in ms
		console.log(deltaTime, deltaTime < duration);
		options.percent = Math.min(   deltaTime / duration   , 1);
		stepCallback.apply(null, [options]);

		if (deltaTime < duration && !stop)
			requestID = window.requestAnimationFrame(frame);
		else {
			stop = true;
			if (!launchEnding) {
				launchEnding = true;
				options.done();
			}
		}
	}

	requestID = window.requestAnimationFrame(frame);

	return {
		stop: function() {
			stop = true;
			window.cancelAnimationFrame(requestID);
			if (!launchEnding) {
				launchEnding = true;
				options.done();
			}
		},
		isStoped: function() {
			return stop;
		}
	}
}














